import { html, css, LitElement } from "lit-element";
import "@polymer/paper-input/paper-input.js";
import "@polymer/paper-button/paper-button.js";
class LoginComponent extends LitElement {
  static get properties() {
    return {
     userObject:{type:Object},
     errors:{type:Boolean}
    };
  }

  constructor(){
    super();
    this.userObject={
      name:"",
      password:""
    }
    this.errors=false;
  
  }

  

  static get styles() {
    return css`
    .container{
      display:flex;
      flex-flow:column wrap;
      width:300px;
      justify-content:center;
      padding:10px;
    }

    paper-input{
      --paper-input-container-color:var(--login-input-color,#f48fb1);
      --paper-input-container-focus-color:var(--login-focused-color,#bf5f82);
      --paper-input-container-input-color:var(--login-text-color,#bf5f82);
    }

    .header{
      height:30px;
      background-color:var(--login-header-background,#bf5f82);
      text-align:center;
      display:flex;
      flex-flow:column nowrap;
      justify-content:center;
      color:#fafafa;
      border-top-right-radius: 5px;
      border-top-left-radius: 5px;
    }

    paper-button{
        background-color:var(--login-button-background,#ffc1e3);
        color:#fafafa;
    }
    
    .errors{
      color:red;
    }
    
    `;
  }

  render() {
    return html`
      <div class="container">
        <div class="header">Inicio de sesión</div>
        <paper-input id="userName" label="Usuario"></paper-input>
        <paper-input id="userPassword" type="password" label="Contraseña"></paper-input>
            ${this.errors?
             html`<p class="errors">Introduce todos tus datos changui</p>`:
             html``}
        <paper-button @click="${this.logIn}">Iniciar Sesión</paper-button>
      </div>
    `;
  }

  logIn(event){
    this.userObject.name=this.shadowRoot.querySelector("#userName").value;
    this.userObject.password=this.shadowRoot.querySelector("#userPassword").value;
    if(this.userObject.name.length>0&&this.userObject.password.length>0){
      this.dispatchEvent(new CustomEvent("user-login",{
        bubbles:false,
        composed:false,
        detail:this.userObject
      }));
      this.errors=false;
    }else{
      this.errors=true;
    }
    
  }


}

window.customElements.define("login-component", LoginComponent);
