/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../login-component.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<login-component></login-component>");
    assert.strictEqual(_element.hello, "Hello World!");
  });
});
